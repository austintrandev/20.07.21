package com.task_53_10;

import java.util.ArrayList;

public class CPerson {
	// create contructor 1
	public CPerson(int id, int age, String firstName, String lastName) {
		super();
		this.id = id;
		this.age = age;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public CPerson(int id, int age, String firstName, String lastName, ArrayList<String> cars ) {
		super();
		this.id = id;
		this.age = age;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	private int id = 0;
	private int age = 0;
	private String firstName = "";
	private String lastName = "";
	ArrayList<String> cars = new ArrayList<String>(); // Create an ArrayList object
	public String country = "Viet Nam";

	// create contructor 2
	public CPerson() {
	};

	// H�m getFullnName
	public String getFullName() {
		return this.firstName + " " + this.lastName;
	}
	
	//H�m get Id
	public int getId() {
		return id;
	}
	
	// H�m set id
	public void setId(int id) {
		this.id = id;
	}

	//H�m get Age
	public int getAge() {
		return age;
	}
	// H�m set Age
	public void setAge(int age) {
		this.age = age;
	}
	// H�m get FirstName
	public String getFirstName() {
		return firstName;
	}
	// H�m set FirstName
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	// H�m get LastName
	public String getLastName() {
		return lastName;
	}
	// H�m setLastName
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * th�m danh s�ch cars v�o person n�y
	 * @param cars the cars to set
	 */
	public void addCars(ArrayList<String> cars) {
		this.cars.addAll(cars);
	}
	
	/**
	 * @return the cars
	 */
	public ArrayList<String> getCars() {
		return cars;
	}
	/**
	 * th�m 1 car v�o danh s�ch person n�y.
	 * @param car the car to set
	 */
	public void addCar(String car) {
		this.cars.add(car);
	}
	// contructor print th�ng tin person
	public String printPerson() {
		return this.getFullName() + " " + this.getAge() + " " + this.getId() + " " + this.country + this.cars;
	}

}
